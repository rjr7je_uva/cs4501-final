-- Parser.hs
-- combination Lisp lexer and parser

module Parser (read_expr, read_expr_list) where

import Control.Monad
import Control.Monad.Error
import Text.ParserCombinators.Parsec hiding (spaces)

import Ast

-- token recognizers

-- consume identifiers symbols
symbol :: Parser Char
symbol = oneOf "!#$%&|*+-/:<=>?@^_~"

-- consume spaces
spaces :: Parser ()
spaces = skipMany1 space

-- consume string
parse_string :: Parser LispVal
parse_string = do
	char '"'
	x <- many (noneOf "\"")
	char '"'
	return (LispStr x)

-- consume atom
parse_atom :: Parser LispVal
parse_atom = do
	first <- letter <|> symbol
	rest <- many (letter <|> digit <|> symbol)
	let atom = first:rest in do
		return $ case atom of
			"#t" -> LispBool True
			"#f" -> LispBool False
			_ -> LispAtom atom

-- consume a number
parse_number :: Parser LispVal
parse_number = liftM (LispNum . read) $ many1 digit

-- grammar functions

-- consume a list
parse_list :: Parser LispVal
parse_list = liftM LispList $ sepBy parse_expr spaces

-- consume dotted list
parse_dotted_list :: Parser LispVal
parse_dotted_list = do
	head <- endBy parse_expr spaces
	tail <- char '.' >> spaces >> parse_expr
	return $ LispDottedList head tail

-- consume quote atom
parse_quoted :: Parser LispVal
parse_quoted = do
	char '\''
	x <- parse_expr
	return $ LispList [LispAtom "quote", x]

-- strip parentheses from lists
consume_parens :: Parser LispVal
consume_parens = do
	char '('
	x <- try parse_list <|> parse_dotted_list
	char ')'
	return x

-- combine parsers
parse_expr :: Parser LispVal
parse_expr = parse_atom
	<|> parse_string
	<|> parse_number
	<|> parse_quoted
	-- strip parentheses from list
	<|> consume_parens

read_or_throw :: Parser a -> String -> ThrowsError a
read_or_throw parser input = case parse parser "lisp" input of
	Left err -> throwError (Parser err)
	Right val -> return val

-- read in a single expression
read_expr :: String -> ThrowsError LispVal
read_expr = read_or_throw parse_expr

-- read a list of expressions
read_expr_list :: String -> ThrowsError [LispVal]
read_expr_list = read_or_throw (endBy parse_expr spaces)
