-- Ast.hs
-- defines the List AST algebraic datatype
-- and various utility and error functions

module Ast (
	unwords_list,
	LispError(..), ThrowsError, IOThrowsError, trapError, extract_value,
	lift_throws, run_IO_throws,
	LispVal(..), show_val, Env(..)
) where

import Data.IORef
import System.IO
import Control.Monad.Error
import Text.ParserCombinators.Parsec hiding (spaces)



-- error handling
-- LispError monad definitions
data LispError =
		NumArgs Integer [LispVal]
	| TypeMismatch String LispVal
	| Parser ParseError
	| BadSpecialForm String LispVal
	| NotFunction String String
	| UnboundVar String String
	| Default String


show_error :: LispError -> String
show_error (UnboundVar message varname) =	message ++ ": " ++ varname
show_error (BadSpecialForm message form) =	message ++ ": " ++ show form
show_error (NotFunction message func) =	message ++ ": " ++ show func
show_error (NumArgs expected found) =
	"Expected " ++ show expected ++ " args; found " ++ (show . length) found
show_error (TypeMismatch expected found) =
	"Invalid type: expected " ++ expected ++ ", found " ++ show found
show_error (Parser parseErr) = "Parse error at " ++ show parseErr

instance Show LispError where
	show = show_error

instance Error LispError where
	noMsg = Default "An error has occured"
	strMsg = Default


type ThrowsError = Either LispError
type IOThrowsError = ErrorT LispError IO

-- consumes potential errors and converts them to string representations
trapError action = catchError action (return . show)

extract_value :: ThrowsError a -> a
extract_value (Right val) = val

lift_throws :: ThrowsError a -> IOThrowsError a
lift_throws (Left err) = throwError err
lift_throws (Right val) = return val

run_IO_throws :: IOThrowsError String -> IO String
run_IO_throws action = runErrorT (trapError action) >>= return . extract_value


type Env = IORef [(String, IORef LispVal)]

-- the AST data tree of a Lisp program
-- note that a lisp program is exactly one expression
data LispVal =
	  LispAtom String
	| LispList [LispVal]
	| LispDottedList [LispVal] LispVal
	| LispNum Int
	| LispStr String
	| LispBool Bool
	| LispPrimFunc ([LispVal] -> ThrowsError LispVal)
	-- params, vararg, body, closure env
	| LispFunc [String] (Maybe String) [LispVal] Env
	| LispIOFunc ([LispVal] -> IOThrowsError LispVal)
	| LispPort Handle

-- utility functions
-- string representation of LispVals
show_val :: LispVal -> String
show_val (LispStr contents) = "\"" ++ contents ++ "\""
show_val (LispAtom name) = name
show_val (LispNum contents) = show contents
show_val (LispBool True) = "#t"
show_val (LispBool False) = "#f"
show_val (LispList contents) = "(" ++ unwords_list contents ++ ")"
show_val (LispDottedList head tail) = "(" ++ unwords_list head ++ " . " ++ show_val tail ++ ")"
show_val (LispPrimFunc _) = "<primitive>"
show_val (LispFunc args varargs body closure) =
  "(lambda (" ++ unwords (map show args) ++ 
     (case varargs of 
        Nothing -> ""
        Just arg -> " . " ++ arg) ++ ") ...)" 
show_val (LispPort _) = "<IO port>"
show_val (LispIOFunc _) = "<IO primitive>"

-- combine together the string representations of LispVals
unwords_list :: [LispVal] -> String
unwords_list = unwords . map show_val

instance Show LispVal where
	show = show_val
