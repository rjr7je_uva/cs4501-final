-- Prolog.hs
-- embedded Prolog language inside Scheme

module Prolog(
	Query, Assertion, Rule, KnowledgeDB, QueryEngine, QueryFrame,
	init_db, init_frame,
	is_fact, is_query, is_rule,
	assert_fact, assert_rule,
	eval_query, query
) where

import Control.Monad.State
import Ast

-- query and assertion is a list of the form
-- (name var/constant var/constant ...)
-- ex. QUERY: (employee (? name) male) == find all male employees
-- ex. ASSERTION (employee bob male) == bob is a male employee
type Query = LispVal
type Assertion = LispVal

-- rule is a list of the form
-- (name conclusion body)
type Rule = LispVal

-- list of assertions and rules
type KnowledgeDB = ([Assertion], [Rule])

-- engine state is knowledge db and rule counter
type QueryEngine = (KnowledgeDB, Int)
init_db = ([], [])

-- frame is a mapping of variables to bindings
type QueryFrame = [(String, LispVal)]
init_frame = []


-- HELPER FUNCTIONS

-- compare query to an assertion
-- queries and assertions should only be lists of ATOMS
query_equals :: Query -> Assertion -> Bool
-- empty lists are equal (base case)
query_equals (LispList []) (LispList []) = True
-- compare atoms
-- OR compare variables
-- ANY two variables are equal, regardless of name
query_equals (LispAtom v1) (LispAtom v2) = v1 == v2
-- uneven lengths automatically make the query and assertion unequal
query_equals (LispList []) (LispList _) = False
query_equals (LispList _) (LispList []) = False
-- recursive definition
query_equals (LispList (qh:qt)) (LispList (ah:at)) = 
	(query_equals qh ah) && (query_equals (LispList qt) (LispList at))
-- else, the query and assertion can't be true
query_equals _ _ = False


-- check if query is a variable
is_var :: Query -> Bool
is_var (LispAtom val) = (head val) == '?' && (length val > 0)
is_var _ = False

-- check if query is a constant
-- i.e., it is an atom but NOT a variable
is_constant :: Query -> Bool
is_constant (LispAtom val) = (not . is_var) (LispAtom val)
is_constant _ = False

-- check if input is a fact
is_fact :: LispVal -> Bool
-- degenerate facts
is_fact (LispList []) = False
is_fact (LispList [x]) = False
-- a properly recursive predicate with no variables
is_fact (LispList (p:rest)) =
	foldr (&&) (is_constant p) (map (\a -> is_fact a || is_constant a) rest)
is_fact _ = False

-- check if input is a valid query
is_query :: LispVal -> Bool
-- degenerate queries
is_query (LispList []) = False
is_query (LispList [x]) = False
-- a properly recursive predicate with no variables
is_query (LispList (p:rest)) =
	foldr (&&) (is_constant p) (map (\a -> is_query a || is_constant a || is_var a) rest)
is_query _ = False

-- check if input is a rule
is_rule :: LispVal -> Bool
is_rule (LispList [LispAtom "rule", conc, LispList []]) = is_query conc
is_rule (LispList [LispAtom "rule", conc, body]) = is_query conc && is_query body
is_rule _ = False

-- get the name of a variable
get_varname :: Query -> String
get_varname (LispAtom val) = tail val

-- rename a variable
rename_var :: Query -> String -> Query
rename_var (LispAtom _) newval = LispAtom newval
rename_var other _ = other


-- get parts of a rule
get_conclusion :: Rule -> Rule
get_conclusion (LispList [LispAtom "rule", conclusion, _]) = conclusion
get_conclusion other = other

get_body :: Rule -> Rule
get_body (LispList [LispAtom "rule", _, body]) = body
get_body other = other


-- check if value is nonempty LispList
nonempty_list :: Query -> Bool
nonempty_list (LispList []) = False
nonempty_list (LispList _) = True
nonempty_list _ = False


-- used to extract values from lists
extract_list :: Query -> [Query]
extract_list (LispList l) = l
extract_list _ = []

extract_head :: Query -> Query
extract_head (LispList (h:_)) = h
extract_head other = other

extract_tail :: Query -> Query
extract_tail (LispList (_:t)) = LispList t
extract_tail _ = LispList []


-- add to database
assert_fact :: KnowledgeDB -> Assertion -> KnowledgeDB
assert_fact db a = (a:(fst db), snd db)

assert_rule :: KnowledgeDB -> Rule -> KnowledgeDB
assert_rule db r = (fst db, r:(snd db))


-- increment rule counter
inc_rule_counter :: State QueryEngine Int
inc_rule_counter = do
	(db, rc) <- get
	put (db, rc+1)
	return (rc+1)


-- PATTERN MATCHING

-- filter out assertions that are not relevant to the query
-- this speeds up the evaluation process
filter_assertions :: Query -> QueryFrame -> State QueryEngine [Assertion]
filter_assertions q frame = do
	((assertions, rules), _) <- get
	-- DUMMY! fill this in later
	return assertions


-- extend frame
extend_if_consistent :: Query -> QueryFrame -> Assertion -> State QueryEngine (Maybe QueryFrame)
extend_if_consistent var frame assert = do
	let varname = get_varname var in do
		case lookup varname frame of
			-- variable was not binded in the frame; add new binding
			Nothing -> return $ Just (frame ++ [(varname, assert)])
			-- recursively pattern match binding as well to 
			Just binding -> do
				pattern <- pattern_match binding frame assert
				case pattern of
					-- if we can't pattern match the binding with the rest of the assertion,
					-- then we can't pattern match the query with the assertion
					Nothing -> return Nothing
					-- successfully matched binding
					Just result -> return $ Just result


-- pattern match query and assertion
pattern_match :: Query -> QueryFrame -> Assertion -> State QueryEngine (Maybe QueryFrame)
pattern_match q frame assert
	-- query matches assertion; return frame without updating it
	| query_equals q assert = return (Just frame)
	-- variable; add binding to frame if consistent
	| is_var q = extend_if_consistent q frame assert
	-- recursive call
	| nonempty_list q && nonempty_list assert = do
		head_result <- pattern_match (extract_head q) frame (extract_head assert)
		case head_result of
			Nothing -> return Nothing
			Just new_frame -> do
				result <- pattern_match (extract_tail q) new_frame (extract_tail assert)
				return result
	-- malformed query
	| otherwise = return Nothing


-- generate frames from pattern matching for a particular frame
check_assertion :: Query -> QueryFrame -> Assertion -> State QueryEngine [QueryFrame]
check_assertion q frame assert = do
	match_result <- pattern_match q frame assert
	case match_result of
		Nothing -> return []
		Just result -> return [result]


-- generate frames from pattern matching
find_assertions :: Query -> QueryFrame -> State QueryEngine [QueryFrame]
find_assertions q frame = do
	assertions <- filter_assertions q frame
	result <- mapM (check_assertion q frame) assertions
	-- flatten result from [[QueryFrame]] to [QueryFrame]
	return (result >>= (\x -> x))


-- UNIFICATION

-- filter out rules that are not relevant to the query
-- this speeds up the evaluation process
filter_rules :: Query -> QueryFrame -> State QueryEngine [Rule]
filter_rules q frame = do
	((assertions, rules), _) <- get
	-- DUMMY! fill this in later
	return rules


-- rename variables in a rule/rule item using the rule counter
rename_vars:: LispVal -> State QueryEngine LispVal
rename_vars item
	-- rename variable
	| is_var item = do
		(_, rc) <- get
		return $ LispAtom ("?" ++ (get_varname item) ++ "-" ++ (show rc))
	-- recursive call
	| nonempty_list item = do
		result <- mapM (rename_vars) (extract_list item)
		return $ LispList result
	| otherwise = return item


-- check if possible binding to variable contains the variable
depends_on :: LispVal -> QueryFrame -> LispVal -> Bool
depends_on exp frame var
	| is_var exp =
		if query_equals exp var
			then True
			else case lookup (get_varname exp) frame of
				Just binding -> depends_on binding frame var
				Nothing -> False
	| nonempty_list exp =
		(depends_on (extract_head exp) frame var) || (depends_on (extract_tail exp) frame var)
	| otherwise = False


-- analogous to extend_if_consistent, but for unification
extend_if_possible :: LispVal -> QueryFrame -> LispVal -> State QueryEngine (Maybe QueryFrame)
extend_if_possible var frame val = do
	case lookup (get_varname var) frame of
		Just var_binding -> unify_match var_binding frame val
		Nothing ->
			if is_var val
				then case lookup (get_varname val) frame of
					Just val_binding -> unify_match var frame val_binding
					Nothing -> return $ Just (frame ++ [(get_varname var, val)])
				else if depends_on val frame var
					then return Nothing
					else return $ Just (frame ++ [(get_varname var, val)])


-- unify query with rule
-- almost the same as pattern_match, except variables are allowed
-- in rules while they are not in assertions
unify_match :: Query -> QueryFrame -> Rule -> State QueryEngine (Maybe QueryFrame)
unify_match q frame rule
	-- query matches rule; return frame without extending it
	| query_equals q rule = return (Just frame)
	-- variable in query
	| is_var q = extend_if_possible q frame rule
	-- variable in rule
	| is_var rule = extend_if_possible rule frame q
	-- recurisve call
	| (nonempty_list q) && (nonempty_list rule) = do
		head_result <- unify_match (extract_head q) frame (extract_head rule)
		case head_result of
			Nothing -> return Nothing
			Just new_frame -> do
				result <- unify_match (extract_tail q) new_frame (extract_tail rule)
				return result
	-- malformed query
	| otherwise = return Nothing


-- apply a single rule to a particular frame
apply_rule :: Query -> QueryFrame -> Rule -> State QueryEngine [QueryFrame]
apply_rule q frame rule = do
	-- update rule counter
	inc_rule_counter
	-- rename variables in rule
	clean_rule <- rename_vars rule
	-- unify query with conclusion of rule
	unify_result <- unify_match q frame (get_conclusion clean_rule)
	case unify_result of
		Nothing -> return []
		Just new_frame -> eval_query (get_body clean_rule) [new_frame]


-- generate frames from unification
apply_rules :: Query -> QueryFrame -> State QueryEngine [QueryFrame]
apply_rules q frame = do
	-- DUMMY! fill this in later
	rules <- filter_rules q frame
	result <- mapM (apply_rule q frame) rules
	return (result >>= (\x -> x))


-- QUERY EVALUATION

-- evaluate a query relative to a particular frame
eval_query_frame :: Query -> QueryFrame -> State QueryEngine [QueryFrame]
eval_query_frame q frame = do
	assertion_frames <- find_assertions q frame
	rule_frames <- apply_rules q frame
	return (assertion_frames ++ rule_frames)


-- evaluate query
-- given a query and an initial binding frame
-- return a list of updated binding frames
eval_query :: Query -> [QueryFrame] -> State QueryEngine [QueryFrame]
eval_query q frames = do
	result <- mapM (eval_query_frame q) frames
	-- flatten result from [[QueryFrame]] to [QueryFrame]
	-- then, convert list of frames into a LispList
	return $ result >>= (\x -> x)


-- convert a list of frames into a LispList
frames_to_list :: [QueryFrame] -> LispVal
frames_to_list frames =
	let convert_binding = (\(var,binding) -> LispList [LispAtom ("?" ++ var), binding]) in
		LispList (map (\frame -> LispList (map convert_binding frame)) frames)


-- rewrite query variables in terms of the *values* of internal variables,
-- not the internal variables themselves
fold_var :: QueryFrame -> LispVal -> LispVal
fold_var frame var =
	-- check if variable can be rewritten in terms of other variables
	case lookup (get_varname var) frame of
		Nothing -> var
		Just val -> if is_var val then fold_var frame val else val


-- extract variables from a query
get_vars :: LispVal -> [LispVal]
get_vars (LispList l) = l >>= get_vars
get_vars var@(LispAtom _) = if is_var var then [var] else []
get_vars _ = []


-- interface to query engine
query :: Query -> State QueryEngine LispVal
query q = do
	frames <- eval_query q [init_frame]
	let vars = get_vars q in do
		return $ LispList (map (\f -> LispList (map (\v -> LispList [v, (fold_var f v)]) vars)) frames)
