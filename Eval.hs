-- Eval.hs
-- evaluate Lisp expressions

module Eval(
	apply, load, eval,
	make_func, make_normal_func, make_varargs
) where

import Control.Monad.Error
import Control.Monad.State
import Data.IORef

import Ast
import Parser
import Env
import Prolog


-- apply arguments to a function
apply :: IORef KnowledgeDB -> LispVal -> [LispVal] -> IOThrowsError LispVal
apply dbref (LispPrimFunc func) args =  lift_throws (func args)
apply dbref (LispFunc params varargs body closure) args = 
  if num params /= num args && varargs == Nothing
		then throwError $ NumArgs (num params) args
		else (liftIO $ bind_vars closure $ zip params args) >>= bind_varargs varargs >>= eval_body dbref
  where remaining_args = drop (length params) args
        num = toInteger . length
        eval_body dbref env = liftM last $ mapM (eval (env, dbref)) body 
        bind_varargs arg env = case arg of
            Just argName -> liftIO $ bind_vars env [(argName, LispList $ remaining_args)]
            Nothing -> return env 
apply dbref (LispIOFunc func) args = func args

-- helper functions to evaluate function/lambda definitions
make_func varargs env params body = return (LispFunc (map show_val params) varargs body env)
make_normal_func = make_func Nothing
make_varargs = make_func . Just . show_val


-- read and parse a list of expressions
load :: String -> IOThrowsError [LispVal]
load filename = (liftIO $ readFile filename) >>= lift_throws . read_expr_list


-- eval function
-- defines operational semantics of Lisp
eval :: (Env, IORef KnowledgeDB) -> LispVal -> IOThrowsError LispVal
-- primitive expressions
eval (env, dbref) val@(LispStr _) = return val
eval (env, dbref) val@(LispNum _) = return val
eval (env, dbref) val@(LispBool _) = return val

-- reference existing variable
eval (env, dbref) (LispAtom id) = get_var env id

-- quoted expression
eval (env, dbref) (LispList [LispAtom "quote", val]) = return val

-- control structures
-- if-then-else
eval (env, dbref) (LispList [LispAtom "if", pred, then_branch, else_branch]) = do
	pred_val <- eval (env, dbref) pred
	case pred_val of
		LispBool True -> do
			then_val <- eval (env, dbref) then_branch
			return then_val
		LispBool False -> do
			else_val <- eval (env, dbref) else_branch
			return else_val
		_ -> do
			throwError (TypeMismatch "LispBool" pred_val)

-- set existing variable
eval (env, dbref) (LispList [LispAtom "set!", LispAtom var, rhs]) = 
	eval (env, dbref) rhs >>= set_var env var

-- define new variable
eval (env, dbref) (LispList [LispAtom "define", LispAtom var, rhs]) =
	eval (env, dbref) rhs >>= define_var env var

-- define function
eval (env, dbref) (LispList (LispAtom "define" : LispList (LispAtom var : params) : body)) =
  make_normal_func env params body >>= define_var env var

-- define function with variable arguments
eval (env, dbref) (LispList (LispAtom "define" : LispDottedList (LispAtom var : params) varargs : body)) =
  make_varargs varargs env params body >>= define_var env var

-- PROLOG FUNCTIONS
-- assert a new fact or rule and add it to the database
eval (env, dbref) (LispList [LispAtom "assert!", a]) = do
	if is_fact a
		then do
			db <- liftIO (readIORef dbref)
			liftIO (writeIORef dbref (assert_fact db a))
			return a
		else do
			if is_rule a
			then do
				db <- liftIO (readIORef dbref)
				liftIO (writeIORef dbref (assert_rule db a))
				return a
			else throwError (BadSpecialForm "Not a valid Prolog rule or fact" a)

-- query the Prolog engine
eval (env, dbref) (LispList [LispAtom "query", q]) = do
	if is_query q
		then do
			db <- liftIO (readIORef dbref)
			return (evalState (query q) (db, 0))
		else throwError (BadSpecialForm "Not a valid Prolog query" q)

-- print out the contents of the database
eval (env, dbref) (LispList [LispAtom "listing"]) = do
	db <- liftIO (readIORef dbref)
	return $ LispList ((fst db) ++ (snd db))

-- define anonymous lambda function
eval (env, dbref) (LispList (LispAtom "lambda" : LispList params : body)) =
  make_normal_func env params body

-- define anonymous lambda function with variable arguments
eval (env, dbref) (LispList (LispAtom "lambda" : LispDottedList params varargs : body)) =
  make_varargs varargs env params body

-- define lambda function with undefined number of arguments
eval (env, dbref) (LispList (LispAtom "lambda" : varargs@(LispAtom _) : body)) =
  make_varargs varargs env [] body

-- load function
eval (env, dbref) (LispList [LispAtom "load", LispStr filename]) = 
  load filename >>= liftM last . mapM (eval (env, dbref))

-- execute function
eval (env, dbref) (LispList (fname : args)) = do
	func <- eval (env, dbref) fname
	arg_vals <- mapM (eval (env, dbref)) args
	apply dbref func arg_vals

-- syntax error
eval (env, dbref) bad = throwError (BadSpecialForm "Unrecognized specifial form" bad)

