{-# LANGUAGE ExistentialQuantification #-}

-- StandardLib.hs
-- implements standard library function

module StandardLib(primitives, io_primitives, load) where

import Control.Monad.Error
import Data.IORef
import System.IO

import Ast
import Parser
import Eval
import Prolog

-- primitive arithmetic / boolean operators

-- Note: this implementation of Scheme is STRONGLY TYPED so
-- the unpack functions won't perform implicit type coercion

-- extracts number from LispNums
unpack_num :: LispVal -> ThrowsError Int
unpack_num (LispNum n) = return n
unpack_num not_num = throwError (TypeMismatch "LispNum" not_num)

-- extracts string from LispStrs
unpack_str :: LispVal -> ThrowsError String
unpack_str (LispStr s) = return s
unpack_str not_str = throwError (TypeMismatch "LispStr" not_str)

-- extracts bool from LispBools
unpack_bool  :: LispVal -> ThrowsError Bool
unpack_bool (LispBool b) = return b
unpack_bool not_bool = throwError (TypeMismatch "LispBool" not_bool)


-- polymorphic function for binary numeric operations
num_binary_op :: (Int -> Int -> Int) -> [LispVal] -> ThrowsError LispVal
num_binary_op f [] = throwError (NumArgs 2 [])
num_binary_op f val@[_] = throwError (NumArgs 2 val)
num_binary_op f args = mapM unpack_num args >>= return . LispNum . foldl1 f

-- polymorphic binary operation that returns boolean
bool_binop :: (LispVal -> ThrowsError a) -> (a -> a -> Bool) -> [LispVal] -> ThrowsError LispVal
bool_binop unpacker op args = do
	if length args /= 2 then do
		throwError (NumArgs 2 args)
	else do
		left <- unpacker (args !! 0)
		right <- unpacker (args !! 1)
		return (LispBool (op left right))

num_bool_binop = bool_binop unpack_num
str_bool_binop = bool_binop unpack_str
bool_bool_binop = bool_binop unpack_bool


-- list primitives
car :: [LispVal] -> ThrowsError LispVal
car [LispList (x:xs)] = return x
car [LispDottedList (x:xs) _] = return x
car [bad_args] = throwError (TypeMismatch "LispList" bad_args)
car bad_args = throwError (NumArgs 1 bad_args)

cdr :: [LispVal] -> ThrowsError LispVal
cdr [LispList (x:xs)] = return (LispList xs)
cdr [LispDottedList [_] x] = return x
cdr [LispDottedList (_:xs) x] = return (LispDottedList xs x)
cdr [bad_args] = throwError (TypeMismatch "LispList" bad_args)
cdr bad_args = throwError (NumArgs 1 bad_args)

cons :: [LispVal] -> ThrowsError LispVal
cons [x1, LispList []] = return (LispList [x1])
cons [x, LispList xs] = return (LispList (x:xs))
cons [x, LispDottedList xs xlast] = return (LispDottedList (x:xs) xlast)
cons [x1, x2] = return (LispDottedList [x1] x2)
cons bad_args = throwError (NumArgs 2 bad_args)

-- equivalence predicates
eqv :: [LispVal] -> ThrowsError LispVal
eqv [(LispBool arg1), (LispBool arg2)] = return (LispBool (arg1 == arg2))
eqv [(LispNum arg1), (LispNum arg2)] = return (LispBool (arg1 == arg2))
eqv [(LispStr arg1), (LispStr arg2)] = return (LispBool (arg1 == arg2))
eqv [(LispAtom arg1), (LispAtom arg2)] = return (LispBool (arg1 == arg2))
eqv [(LispDottedList xs x), (LispDottedList ys y)] =
	eqv [LispList (xs ++ [x]), LispList $ ys ++ [y]]
eqv [(LispList arg1), (LispList arg2)] =
	return (LispBool ((length arg1 == length arg2) && (all eqvPair (zip arg1 arg2))))
  where eqvPair (x1, x2) = case eqv [x1, x2] of
															Left err -> False
															Right (LispBool val) -> val
eqv [_, _] = return (LispBool False)
eqv bad_args = throwError (NumArgs 2 bad_args)


data Unpacker = forall a. Eq a => AnyUnpacker (LispVal -> ThrowsError a)

unpackEquals :: LispVal -> LispVal -> Unpacker -> ThrowsError Bool
unpackEquals arg1 arg2 (AnyUnpacker unpacker) = 
             do unpacked1 <- unpacker arg1
                unpacked2 <- unpacker arg2
                return $ unpacked1 == unpacked2
        `catchError` (const $ return False)

equal :: [LispVal] -> ThrowsError LispVal
equal [arg1, arg2] = do
    primitiveEquals <- liftM or $ mapM (unpackEquals arg1 arg2) 
                      [AnyUnpacker unpack_num, AnyUnpacker unpack_str, AnyUnpacker unpack_bool]
    eqvEquals <- eqv [arg1, arg2]
    return (LispBool ((primitiveEquals || let (LispBool x) = eqvEquals in x)))
equal badArgList = throwError $ NumArgs 2 badArgList

-- list of primitive arithmetic/boolean functions
primitives :: [(String, [LispVal] -> ThrowsError LispVal)]
primitives = [("+", num_binary_op (+)),
							("-", num_binary_op (-)),
							("*", num_binary_op (*)),
							("/", num_binary_op div),
							("mod", num_binary_op mod),
							("quotient", num_binary_op quot),
							("remainder", num_binary_op rem),
              ("=", num_bool_binop (==)),
              ("<", num_bool_binop (<)),
              (">", num_bool_binop (>)),
              ("/=", num_bool_binop (/=)),
              (">=", num_bool_binop (>=)),
              ("<=", num_bool_binop (<=)),
              ("&&", bool_bool_binop (&&)),
              ("||", bool_bool_binop (||)),
              ("string=?", str_bool_binop (==)),
              ("string<?", str_bool_binop (<)),
              ("string>?", str_bool_binop (>)),
              ("string<=?", str_bool_binop (<=)),
              ("string>=?", str_bool_binop (>=)),
              ("car", car),
              ("cdr", cdr),
              ("cons", cons),
              ("eqv?", eqv),
              ("equal?", equal)
						]

-- IO func version of apply
-- this function is broken because of the Prolog engine. fix later...
-- apply_proc :: IORef KnowledgeDB -> [LispVal] -> IOThrowsError LispVal
-- apply_proc db [func, LispList args] = apply db func args
-- apply_proc db (func : args) = apply db func args

-- open a handle
make_port :: IOMode -> [LispVal] -> IOThrowsError LispVal
make_port mode [LispStr filename] = liftM LispPort (liftIO (openFile filename mode))

-- close a handle
close_port :: [LispVal] -> IOThrowsError LispVal
close_port [LispPort port] = liftIO (hClose port >> (return (LispBool True)))
close_port _ = return (LispBool False)

-- read from handle
read_proc :: [LispVal] -> IOThrowsError LispVal
read_proc [] = read_proc [LispPort stdin]
read_proc [LispPort port] = (liftIO (hGetLine port)) >>= lift_throws . read_expr

-- write to handle
write_proc :: [LispVal] -> IOThrowsError LispVal
write_proc [obj] = write_proc [obj, LispPort stdout]
write_proc [obj, LispPort port] = liftIO (hPrint port obj >> (return (LispBool True)))

-- read a line from handle
read_contents :: [LispVal] -> IOThrowsError LispVal
read_contents [LispStr filename] = liftM LispStr (liftIO (readFile filename))

-- wrapper for load
read_all :: [LispVal] -> IOThrowsError LispVal
read_all [LispStr filename] = liftM LispList (load filename)

-- list of IO primitive functions
io_primitives :: [(String, [LispVal] -> IOThrowsError LispVal)]
io_primitives = [--("apply", apply_proc),
                ("open-input-file", make_port ReadMode),
                ("open-output-file", make_port WriteMode),
                ("close-input-port", close_port),
                ("close-output-port", close_port),
                ("read", read_proc),
                ("write", write_proc),
                ("read-contents", read_contents),
                ("read-all", read_all)]