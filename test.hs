let db = ([LispList [LispAtom "employee", LispAtom "bob", LispAtom "fred"]], [LispList [LispAtom "rule", LispList [LispAtom "boss", LispAtom "?X", LispAtom "?Y"], LispList[LispAtom "employee", LispAtom "?Y", LispAtom "?X"]]])

let q = LispList [LispAtom "boss", LispAtom "?name", LispAtom "bob"]
let q2 = LispList [LispAtom "employee", LispAtom "bob", LispAtom "?X"]

let r = LispList [LispAtom "rule", LispList [LispAtom "boss", LispAtom "?X", LispAtom "?Y"], LispList[LispAtom "employee", LispAtom "?Y", LispAtom "?X"]]
let a = LispList [LispAtom "employee", LispAtom "bob", LispAtom "fred"]

add(X, 0, X)
add(X, succ(Y), succ(Z)) :- add(X, Y, Z)

let db2 = ([], [LispList [LispAtom "rule", LispList [LispAtom "add", LispAtom "?X", LispAtom "0", LispAtom "?X"], LispList []], LispList [LispAtom "rule", LispList [LispAtom "add", LispAtom "?X", LispList [LispAtom "succ", LispAtom "?Y"], LispList [LispAtom "succ", LispAtom "?Z"]], LispList [LispAtom "add", LispAtom "?X", LispAtom "?Y", LispAtom "?Z"]]])
let q3 = LispList [LispAtom "add", LispList [LispAtom "succ", LispList [LispAtom "succ", LispAtom "0"]], LispList [LispAtom "succ", LispList [LispAtom "succ", LispAtom "0"]], LispAtom "?sum"]