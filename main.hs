-- main.hs

import System.Environment
import System.IO
import Data.IORef
import Control.Monad

import Ast
import Parser
import Env
import Eval
import StandardLib
import Prolog


-- mutable knowledge db
null_db :: IO (IORef KnowledgeDB)
null_db = newIORef init_db

-- null env with primitive function bindings
primitive_bindings :: IO Env
primitive_bindings = null_env >>= ((flip bind_vars) ((map (make_func_binding LispIOFunc) io_primitives)
			                                            ++ (map (make_func_binding LispPrimFunc) primitives)))
  where make_func_binding constructor (var, func) = (var, constructor func)

flush_str :: String -> IO ()
flush_str str = putStr str >> hFlush stdout

read_prompt :: String -> IO String
read_prompt prompt = flush_str prompt >> getLine

eval_str :: (Env, IORef KnowledgeDB) -> String -> IO String
eval_str envdb expr = run_IO_throws $ liftM show $ (lift_throws $ read_expr expr) >>= eval envdb

eval_and_print :: Env -> IORef KnowledgeDB -> String -> IO ()
eval_and_print env db expr = eval_str (env, db) expr >>= putStrLn

until_ :: Monad m => (a -> Bool) -> m a -> (a -> m ()) -> m ()
until_ pred prompt action = do 
  result <- prompt
  if pred result 
     then return ()
     else action result >> until_ pred prompt action

run_one :: [String] -> IO ()
run_one args = do
    env <- primitive_bindings >>= flip bind_vars [("args", LispList $ map LispStr $ drop 1 args)] 
    db <- null_db
    (run_IO_throws $ liftM show $ eval (env, db) (LispList [LispAtom "load", LispStr (args !! 0)])) 
         >>= hPutStrLn stderr

run_repl :: IO ()
run_repl = do
  db <- null_db
  env <- primitive_bindings
  (until_ (== "quit") (read_prompt "Lisp>>> ")) (eval_and_print env db)

main = do
	-- get arguments
	args <- getArgs
	if null args then run_repl else run_one $ args
