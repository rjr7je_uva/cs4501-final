(define (not x)            (if x #f #t))
(define (null? obj)        (if (eqv? obj '()) #t #f))
(define (list . objs)       objs)
(define (id obj)           obj)
(define (flip func)        (lambda (arg1 arg2) (func arg2 arg1)))
(define (curry func arg1)  (lambda (arg) (apply func (cons arg1 (list arg)))))
(define (compose f g)      (lambda (arg) (f (apply g arg))))

(define zero?              (curry = 0))
(define positive?          (curry < 0))
(define negative?          (curry > 0))
(define (odd? num)         (= (mod num 2) 1))
(define (even? num)        (= (mod num 2) 0))

(define (foldr func end lst)
  (if (null? lst)
      end
      (func (car lst) (foldr func end (cdr lst)))))

(define (foldl func accum lst)
  (if (null? lst)
      accum
      (foldl func (func accum (car lst)) (cdr lst))))

(define fold foldl)
(define reduce foldr)

(define (unfold func init pred)
  (if (pred init)
      (cons init '())
      (cons init (unfold func (func init) pred))))

(define (sum . lst)         (fold + 0 lst))
(define (product . lst)     (fold * 1 lst))
(define (and . lst)         (fold && #t lst))
(define (or . lst)          (fold || #f lst))

(define (max first . rest) (fold (lambda (old new) (if (> old new) old new)) first rest))
(define (min first . rest) (fold (lambda (old new) (if (< old new) old new)) first rest))
(define (length lst)        (fold (lambda (x y) (+ x 1)) 0 lst))
(define (reverse lst)       (fold (flip cons) '() lst))
(define (mem-helper pred op) (lambda (acc next) (if (and (not acc) (pred (op next))) next acc)))
(define (memq obj lst)       (fold (mem-helper (curry eq? obj) id) #f lst))
(define (memv obj lst)       (fold (mem-helper (curry eqv? obj) id) #f lst))
(define (member obj lst)     (fold (mem-helper (curry equal? obj) id) #f lst))
(define (assq obj alist)     (fold (mem-helper (curry eq? obj) car) #f alist))
(define (assv obj alist)     (fold (mem-helper (curry eqv? obj) car) #f alist))
(define (assoc obj alist)    (fold (mem-helper (curry equal? obj) car) #f alist))

(define (map func lst)      (foldr (lambda (x y) (cons (func x) y)) '() lst))
(define (filter pred lst)   (foldr (lambda (x y) (if (pred x) (cons x y) y)) '() lst))

(define (extend-if-possible var val frame)
	(let ((binding (binding-in-frame var frame)))
		(cond (binding (unify-match (binding-value binding) val frame))
			((var? val)
				(let ((binding (binding-in-frame val frame)))
					(if binding
						(unify-match var (binding-value binding) frame)
						(extend var val frame)
					)
				)
			)
			((depends-on? val var frame) 'failed)
			(else (extend var val frame))
		)
	)
)

(define (depends-on? exp var frame)
	(define (tree-walk e)
		(cond ((var? e)
				(if (equal? var e)
					true
					(let ((b (binding-in-frame e frame)))
						(if b
							(tree-walk (binding-value b))
							false
						)
					)
				)
			)
			((pair? e) (or (tree-walk (car e)) (tree-walk (cdr e))))
			(else false)
		)
	)
	(tree-walk exp))

(define (apply-a-rule rule query-pattern query-frame)
	(let ((clean-rule (rename-variables-in rule)))
		(let ((unify-result
				(unify-match query-pattern (conclusion clean-rule) query-frame)))
			(if (eq? unify-result 'failed)
				the-empty-stream
				(qeval (rule-body clean-rule) (singleton-stream unify-result))
			)
		)
	)
)