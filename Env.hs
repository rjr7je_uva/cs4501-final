-- Env.hs
-- environment

module Env(
	Env, IOThrowsError,
	null_env, lift_throws, run_IO_throws, is_bound,
	get_var, set_var, define_var, bind_vars
) where

import Data.IORef
import System.IO
import Control.Monad
import Control.Monad.Error

import Ast

null_env :: IO Env
null_env = newIORef []

is_bound :: Env -> String -> IO Bool
is_bound env_ref var = readIORef env_ref >>= return . maybe False (const True) . lookup var

get_var :: Env -> String -> IOThrowsError LispVal
get_var env_ref var  = do
	env <- liftIO (readIORef env_ref)
	maybe (throwError (UnboundVar "Getting an unbound variable" var))
				(liftIO . readIORef)
				(lookup var env)

set_var :: Env -> String -> LispVal -> IOThrowsError LispVal
set_var env_ref var value = do
	env <- liftIO (readIORef env_ref)
	maybe (throwError (UnboundVar "Setting an unbound variable" var))
				(liftIO . (flip writeIORef value))
				(lookup var env)
	return value

define_var :: Env -> String -> LispVal -> IOThrowsError LispVal
define_var env_ref var value = do 
		already_defined <- liftIO (is_bound env_ref var)
		if already_defined 
		then set_var env_ref var value >> return value
		else liftIO $ do 
		  value_ref <- newIORef value
		  env <- readIORef env_ref
		  writeIORef env_ref ((var, value_ref) : env)
		  return value

bind_vars :: Env -> [(String, LispVal)] -> IO Env
bind_vars env_ref bindings = readIORef env_ref >>= extendEnv bindings >>= newIORef
    where extendEnv bindings env = liftM (++ env) (mapM addBinding bindings)
          addBinding (var, value) = do ref <- newIORef value
                                       return (var, ref)